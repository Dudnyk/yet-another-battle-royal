package pl.vdudnyk.game.network;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import com.esotericsoftware.kryonet.Server;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import pl.vdudnyk.game.common.ObjectRegistry;
import pl.vdudnyk.game.common.Status;
import pl.vdudnyk.game.common.request.BaseRequest;
import pl.vdudnyk.game.common.request.GetServerStatusRequest;
import pl.vdudnyk.game.common.response.BaseResponse;
import pl.vdudnyk.game.common.response.GetServerStatusResponse;

import java.io.IOException;
import java.util.Date;


/**
 * Created by vadym on 08.10.17.
 */
@Component
public class KryonetServer{
    private static final Logger log = LoggerFactory.getLogger(KryonetServer.class);
    private Server server;

    public void start() {
        server = new Server();
        Kryo kryo = server.getKryo();
        new ObjectRegistry().register(kryo);
        server.start();
        try {
            server.bind(54555, 54777);
            log.info("Kryonet server started");
        } catch (IOException e) {
            log.info("Cannot bind ip addresses");
        }

        server.addListener(new Listener() {
            public void received (Connection connection, Object object) {
                if(object instanceof GetServerStatusRequest) {
                    log.info("Received: GetServerStatusRequest");
                    GetServerStatusResponse response = new GetServerStatusResponse();
                    response.setDate(new Date());
                    response.setStatus(Status.SUCCESS);
                    connection.sendTCP(response);
                }
            }
        });
    }

    public Server getServer() {
        return server;
    }
}
