package pl.vdudnyk.game;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ServerApplication {
	public static final int MAX_PLAYERS_IN_ROOM = 3;

	public static void main(String[] args) {
		SpringApplication.run(ServerApplication.class, args);
	}
}
