package pl.vdudnyk.game.world;

import org.springframework.stereotype.Service;
import pl.vdudnyk.game.common.dto.Player;
import pl.vdudnyk.game.common.dto.gaming.Health;
import pl.vdudnyk.game.common.dto.gaming.PlayerState;
import pl.vdudnyk.game.common.dto.gaming.Position;

import java.util.Random;

@Service
public class PlayerService {
    public Player createPlayer(Player player) {
        player.setPlayerState(new PlayerState());

        player.getPlayerState().setPosition(generatePosition());
        player.getPlayerState().setHealth(generateHealth());
        return player;
    }

    private Health generateHealth() {
        Health health = new Health();
        health.setCurrent(80);
        health.setMax(100);
        return health;
    }

    private Position generatePosition() {
        Position position = new Position();
        Random random = new Random();
        position.setX(100 + random.nextInt(200));
        position.setY(100 + random.nextInt(200));
        return position;
    }

}
