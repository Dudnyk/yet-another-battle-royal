package pl.vdudnyk.game.world;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.vdudnyk.game.common.dto.GameRoom;
import pl.vdudnyk.game.common.dto.GameWorld;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by vadym on 28.11.17.
 */
@Component
public class GameWorldPool {
    private Map<Long, GameWorld> gameWorlds = new HashMap<>();

    private CreateGameWorldService createGameWorldService;

    @Autowired
    public GameWorldPool(CreateGameWorldService createGameWorldService) {
        this.createGameWorldService = createGameWorldService;
    }

    public GameWorld getOrCreateGameWorld(GameRoom gameRoom) {
        if (gameWorlds.get(gameRoom.getGameRoomId()) == null) {
            GameWorld newGameWorld = createGameWorldService.createNewGameWorld(gameRoom);
            gameWorlds.put(newGameWorld.getGameWorldId(), newGameWorld);
        }
        return gameWorlds.get(gameRoom.getGameRoomId());
    }

    public GameWorld getGameWorld(long gameWorldId) {
        return gameWorlds.get(gameWorldId);
    }
}
