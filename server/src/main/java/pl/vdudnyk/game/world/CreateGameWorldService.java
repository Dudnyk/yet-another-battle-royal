package pl.vdudnyk.game.world;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.vdudnyk.game.common.dto.GameRoom;
import pl.vdudnyk.game.common.dto.GameWorld;
import pl.vdudnyk.game.common.dto.Player;
import pl.vdudnyk.game.common.dto.gaming.Position;

import java.util.stream.Collectors;

/**
 * Created by vadym on 28.11.17.
 */
@Component
public class CreateGameWorldService {

    @Autowired
    private PlayerService playerService;

    public GameWorld createNewGameWorld(GameRoom gameRoom) {
        GameWorld gameWorld = new GameWorld();
        gameWorld.setPlayers(gameRoom.getPlayers().stream().collect(Collectors.toMap(Player::getNickname, player -> player)));
        gameWorld.setGameWorldId(gameRoom.getGameRoomId());

        spawnPlayers(gameWorld);

        return gameWorld;
    }

    private void spawnPlayers(GameWorld gameWorld) {
        gameWorld.getPlayers().forEach((nickname, player) -> playerService.createPlayer(gameWorld.getPlayers().get(nickname)));
    }


}
