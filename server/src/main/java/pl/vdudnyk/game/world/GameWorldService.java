package pl.vdudnyk.game.world;

import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.vdudnyk.game.common.dto.GameWorld;
import pl.vdudnyk.game.common.dto.Player;
import pl.vdudnyk.game.common.request.GetGameWorldRequest;
import pl.vdudnyk.game.common.request.UpdateGameWorldRequest;
import pl.vdudnyk.game.common.response.GetGameWorldResponse;
import pl.vdudnyk.game.common.response.UpdateGameWorldResponse;
import pl.vdudnyk.game.network.KryonetServer;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Created by vadym on 28.11.17.
 */
@Component
public class GameWorldService {
    private KryonetServer kryonetServer;
    private GameWorldPool gameWorldPool;

    @Autowired
    public GameWorldService(KryonetServer kryonetServer, GameWorldPool gameWorldPool) {
        this.kryonetServer = kryonetServer;
        this.gameWorldPool = gameWorldPool;
        setup();
    }

    private void setup() {
        kryonetServer.getServer().addListener(new Listener() {
            @Override
            public void received(Connection connection, Object object) {
                super.received(connection, object);
                if (object instanceof GetGameWorldRequest) {
                    GetGameWorldRequest request = (GetGameWorldRequest) object;
                    GameWorld gameWorld = gameWorldPool.getOrCreateGameWorld(request.getGameRoom());
                    GetGameWorldResponse getGameWorldResponse = new GetGameWorldResponse();
                    getGameWorldResponse.setGameWorld(gameWorld);
                    kryonetServer.getServer().sendToTCP(connection.getID(), getGameWorldResponse);
                }
            }
        });


        kryonetServer.getServer().addListener(new Listener() {
            @Override
            public void received(Connection connection, Object object) {
                super.received(connection, object);
                if (object instanceof UpdateGameWorldRequest) {
                    UpdateGameWorldRequest request = (UpdateGameWorldRequest) object;
                    GameWorld serverGameWorld = gameWorldPool.getGameWorld(request.getGameWorld().getGameWorldId());
                    GameWorld updatedGameWorld = updateGameWorld(serverGameWorld, request.getGameWorld(), connection.getID());
                    UpdateGameWorldResponse updateGameWorldResponse = new UpdateGameWorldResponse();
                    updateGameWorldResponse.setGameWorld(updatedGameWorld);
                    kryonetServer.getServer().sendToTCP(connection.getID(), updateGameWorldResponse);
                }
            }
        });

    }

    private GameWorld updateGameWorld(GameWorld oldGameWorld, GameWorld newGameWorld, int connectionId) {
        return updateUserPosition(oldGameWorld, newGameWorld, connectionId);
    }

    private GameWorld updateUserPosition(GameWorld oldGameWorld, GameWorld newGameWorld, int connectionId) {
        Optional<Player> updatedPlayer = newGameWorld
                .getPlayers()
                .values()
                .stream()
                .filter(player -> player.getConnectionID() == connectionId)
                .findFirst();
        if (!updatedPlayer.isPresent()) {
            return oldGameWorld;
        }
        oldGameWorld.getPlayers().get(updatedPlayer.get().getNickname()).setPlayerState(updatedPlayer.get().getPlayerState());
        oldGameWorld.setPlayers(oldGameWorld.getPlayers());
        return oldGameWorld;
    }


}
