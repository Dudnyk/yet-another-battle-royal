package pl.vdudnyk.game.room;

import org.springframework.stereotype.Component;
import pl.vdudnyk.game.common.dto.GameRoom;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static pl.vdudnyk.game.ServerApplication.MAX_PLAYERS_IN_ROOM;

/**
 * Created by vadym on 23.10.17.
 */
@Component
public class GameRoomPool {
    private List<GameRoom> gameRooms = new ArrayList<>();

    public GameRoom getGameRoom() {
        Optional<GameRoom> first = gameRooms
                .stream()
                .filter(gameRoom -> gameRoom.getPlayers().size() < gameRoom.getMaxPlayersCount())
                .findFirst();
        if(first.isPresent()) {
            return first.get();
        }

        GameRoom gameRoom = createGameRoom();
        gameRooms.add(gameRoom);
        return gameRoom;
    }

    private GameRoom createGameRoom() {
        GameRoom gameRoom = new GameRoom();
        gameRoom.setPlayers(new ArrayList<>());
        gameRoom.setMaxPlayersCount(MAX_PLAYERS_IN_ROOM);
        gameRoom.setGameRoomId(gameRooms.size()+1);
        return gameRoom;
    }
}
