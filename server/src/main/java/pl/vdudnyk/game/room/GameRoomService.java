package pl.vdudnyk.game.room;

import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.vdudnyk.game.common.dto.GameRoom;
import pl.vdudnyk.game.common.dto.Player;
import pl.vdudnyk.game.common.request.ConnectToGameRoomRequest;
import pl.vdudnyk.game.common.response.ConnectToGameRoomResponse;
import pl.vdudnyk.game.common.response.StartGameResponse;
import pl.vdudnyk.game.network.KryonetServer;

import java.util.UUID;

import static pl.vdudnyk.game.ServerApplication.MAX_PLAYERS_IN_ROOM;

/**
 * Created by vadym on 23.10.17.
 */
@Service
public class GameRoomService {

    private KryonetServer kryonetServer;

    private GameRoomPool gameRoomPool;

    @Autowired
    public GameRoomService(KryonetServer kryonetServer, GameRoomPool gameRoomPool) {
        this.kryonetServer = kryonetServer;
        this.gameRoomPool = gameRoomPool;
        kryonetServer.start();

        setupListeners();

    }

    private void setupListeners() {
        kryonetServer.getServer().addListener(new Listener() {
            @Override
            public void received(Connection connection, Object object) {
                super.received(connection, object);
                if (object instanceof ConnectToGameRoomRequest) {
                    ConnectToGameRoomRequest request = (ConnectToGameRoomRequest) object;
                    GameRoom gameRoom = gameRoomPool.getGameRoom();
                    gameRoom.getPlayers().add(createPlayer(connection, request));
                    sendResponsesToAllMembersInRoom(gameRoom);
                    if(gameRoom.getPlayers().size() == MAX_PLAYERS_IN_ROOM) {
                        sendStartGame(gameRoom);
                    }
                }
            }
        });
    }

    private Player createPlayer(Connection connection, ConnectToGameRoomRequest request) {
        Player player = new Player();
        player.setConnectionID(connection.getID());
        player.setPlayerID(UUID.randomUUID().toString());
        player.setNickname(request.getNickname());
        return player;
    }

    private void sendResponsesToAllMembersInRoom(GameRoom gameRoom) {
        gameRoom.getPlayers().forEach(player -> {
            ConnectToGameRoomResponse connectToGameRoomResponse = new ConnectToGameRoomResponse();
            connectToGameRoomResponse.setGameRoom(gameRoom);
            kryonetServer.getServer().sendToTCP(player.getConnectionID(), connectToGameRoomResponse);
        });

    }

    private void sendStartGame(GameRoom gameRoom) {
        gameRoom.getPlayers().forEach(player -> {
            System.out.println("Sending start game to: " + player.getConnectionID());
            StartGameResponse startGameResponse = new StartGameResponse();
            startGameResponse.setGameRoom(gameRoom);
            kryonetServer.getServer().sendToTCP(player.getConnectionID(), startGameResponse);
        });
    }

}
