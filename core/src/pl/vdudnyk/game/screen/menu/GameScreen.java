package pl.vdudnyk.game.screen.menu;

import pl.vdudnyk.game.control.SceneController;
import pl.vdudnyk.game.screen.ui.Styles;

/**
 * Created by vadym on 14.10.17.
 */
public class GameScreen extends BaseScreen {
    public static final String SCREEN_NAME = "GameScreen";

    private SceneController sceneController;


    public GameScreen(Styles styles, SceneController sceneController) {
        super(styles.getSkin(), SCREEN_NAME);
        this.sceneController = sceneController;
    }

    @Override
    public void show() {
        super.show();
        sceneController.init(stage);
    }

    @Override
    public void render(float delta) {
        sceneController.update(delta);
        super.render(delta);
    }

    @Override
    public void resize(int width, int height) {
        stage.getViewport().update(width, height, true);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        stage.dispose();
    }



    @Override
    public void addButtons() {

    }


}
