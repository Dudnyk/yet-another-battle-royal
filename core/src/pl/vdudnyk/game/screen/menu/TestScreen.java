package pl.vdudnyk.game.screen.menu;

import pl.vdudnyk.game.network.NetworkClient;
import pl.vdudnyk.game.screen.ui.Styles;

import javax.inject.Inject;

public class TestScreen extends BaseScreen {

    public static final String SCREEN_NAME = "TestScreen";


    private final NetworkClient networkClient;

    @Inject
    public TestScreen(NetworkClient networkClient, Styles styles) {
        super(styles.getSkin(), SCREEN_NAME);
        this.networkClient = networkClient;
    }

    @Override
    public void addButtons() {
        System.out.println("adding buttons to test screen");
    }
}
