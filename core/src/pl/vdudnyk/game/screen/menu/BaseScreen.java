package pl.vdudnyk.game.screen.menu;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;

/**
 * Created by vadym on 14.10.17.
 */
public abstract class BaseScreen implements Screen {
    public Skin skin;
    public Stage stage;
    public String screenName;
    int numberOfButtons = 0;
    private InputMultiplexer inputMultiplexer = new InputMultiplexer();


    public BaseScreen(Skin skin, String screenName) {
        this.skin = skin;
        this.screenName = screenName;
    }

    final Button addButton(String buttonName) {
        TextButton textButton = new TextButton(buttonName, skin); // Use the initialized skin
        int x = Gdx.graphics.getWidth() / 2 - Gdx.graphics.getWidth() / 8;
        int y = Gdx.graphics.getHeight() / 5 + (numberOfButtons * Gdx.graphics.getHeight() / 5);
        textButton.setPosition(x, y);
        stage.addActor(textButton);
        numberOfButtons++;
        return textButton;
    }

    public abstract void addButtons();

    @Override
    public void show() {
        numberOfButtons = 0;

        stage = new Stage();
        inputMultiplexer.addProcessor(stage);
        Gdx.input.setInputProcessor(inputMultiplexer);// Make the stage consume events

        initBackground(stage);
        addButtons();
    }

    private void initBackground(Stage stage) {
        //Background
        Image image = new Image(new Texture(Gdx.files.internal("background/menu_background1.jpg")));
        image.setWidth(Gdx.graphics.getWidth());
        image.setHeight(Gdx.graphics.getHeight());

        stage.addActor(image);

    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(1, 1, 1, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);


        stage.act(delta);
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        stage.dispose();
    }
}
