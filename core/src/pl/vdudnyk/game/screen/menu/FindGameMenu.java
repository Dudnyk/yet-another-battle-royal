package pl.vdudnyk.game.screen.menu;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.esotericsoftware.kryonet.Client;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import pl.vdudnyk.game.common.request.GetServerStatusRequest;
import pl.vdudnyk.game.common.response.GetServerStatusResponse;
import pl.vdudnyk.game.network.NetworkClient;
import pl.vdudnyk.game.screen.ScreenDirector;
import pl.vdudnyk.game.screen.ui.Styles;

import javax.inject.Inject;

/**
 * Created by vadym on 14.10.17.
 */
public class FindGameMenu extends BaseScreen {
    public static final String SCREEN_NAME = "FindGameMenuScreen";

    private NetworkClient networkClient;

    @Inject
    public FindGameMenu(NetworkClient networkClient, Styles styles) {
        super(styles.getSkin(), SCREEN_NAME);
        this.networkClient = networkClient;
    }

    @Override
    public void addButtons() {
        Button back = addButton("Back");
        Button serverStatus = addButton("Server status");
        Button connect = addButton("Connect to Game");

        back.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                System.out.println("Back");
                ScreenDirector.getInstance().setScreen(MainMenu.SCREEN_NAME);
            }
        });

        serverStatus.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                showServerStatus();
            }
        });

        connect.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                ScreenDirector.getInstance().setScreen(GameRoomMenu.SCREEN_NAME);

            }
        });
    }

    private void showServerStatus() {
        Client client = networkClient.getClient();
        client.addListener(new Listener() {
            public void received(Connection connection, Object object) {
                if (object instanceof GetServerStatusResponse) {
                    GetServerStatusResponse response = (GetServerStatusResponse) object;
                    Label label = new Label(response.getStatus().toString(), skin); // Use the initialized skin
                    int x = Gdx.graphics.getWidth() / 2 - Gdx.graphics.getWidth() / 8;
                    int y = Gdx.graphics.getHeight() / 5 + (numberOfButtons * Gdx.graphics.getHeight() / 5);
                    label.setPosition(x, y);
                    stage.addActor(label);
                }
            }
        });
        client.sendTCP(new GetServerStatusRequest());
    }
}
