package pl.vdudnyk.game.screen.menu;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import pl.vdudnyk.game.screen.ScreenDirector;
import pl.vdudnyk.game.screen.ui.Styles;

import javax.inject.Inject;

/**
 * Created by vadym on 14.10.17.
 */
public class MainMenu extends BaseScreen {

    public static final String SCREEN_NAME = "MainMenuScreen";

    @Inject
    public MainMenu(Styles styles) {
        super(styles.getSkin(), SCREEN_NAME);
    }

    @Override
    public void addButtons() {
        Button exit = addButton("Exit");
        Button preferences = addButton("Preferences");
        Button findGame = addButton("Find Game");

        findGame.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                System.out.println("Find Game Clicked");
                ScreenDirector.getInstance().setScreen(FindGameMenu.SCREEN_NAME);
            }
        });

        preferences.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                System.out.println("preferences");
            }
        });

        exit.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                System.out.println("exit");
                Gdx.app.exit();
            }
        });
    }
}
