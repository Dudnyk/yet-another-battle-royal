package pl.vdudnyk.game.screen.menu;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.List;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import pl.vdudnyk.game.common.dto.GameRoom;
import pl.vdudnyk.game.common.dto.Player;
import pl.vdudnyk.game.network.gameroom.GameRoomFoundListener;
import pl.vdudnyk.game.network.gameroom.GameRoomService;
import pl.vdudnyk.game.network.gameroom.PlayerDataHolder;
import pl.vdudnyk.game.network.gameroom.StartGameListener;
import pl.vdudnyk.game.screen.ScreenDirector;
import pl.vdudnyk.game.screen.ui.Styles;

import javax.inject.Inject;

/**
 * Created by vadym on 23.10.17.
 */
public class GameRoomMenu extends BaseScreen {
    public static final String SCREEN_NAME = "GameRoomMenuScreen";


    private GameRoomService gameRoomService;

    private int playersCount = 0;
    private int maxPlayers = 0;

    Label label;

    @Inject
    public GameRoomMenu(Styles styles, GameRoomService gameRoomService) {
        super(styles.getSkin(), SCREEN_NAME);
        this.gameRoomService = gameRoomService;
    }


    @Override
    public void show() {
        super.show();
        label = new Label("Waiting for players: " + playersCount + "/" + maxPlayers, skin);
        label.setPosition(Styles.DEFAULT_OFFSET, stage.getHeight() - stage.getHeight() / 10);
        label.setFontScale(1.3f);
        label.setLayoutEnabled(true);
        label.setSize(stage.getViewport().getScreenWidth(), 40);
        List<String> connectedPlayersListComponent = new List<String>(skin);
        connectedPlayersListComponent.setPosition(Styles.DEFAULT_OFFSET, stage.getHeight() - stage.getHeight() / 10);
        stage.addActor(label);
        stage.addActor(connectedPlayersListComponent);

        subscribeForGameStart();
        connectToGameRoom(connectedPlayersListComponent);


    }

    private void subscribeForGameStart() {
        gameRoomService.subscribeForGameStart(new StartGameListener() {
            @Override
            public void gameStarted(GameRoom gameRoom) {
                System.out.println("Start Game:" + gameRoom.getPlayers().size());
                PlayerDataHolder.setGameRoom(gameRoom);
                ScreenDirector.getInstance().setScreen(GameScreen.SCREEN_NAME);
            }
        });
    }

    private void connectToGameRoom(final List<String> connectedPlayersListComponent) {
        String nickname = "Player " + (int) (Math.random() * 100);
        PlayerDataHolder.setNickName(nickname);
        gameRoomService.findGameRoom(nickname, new GameRoomFoundListener() {
            @Override
            public void gameRoomFound(GameRoom gameRoom) {
                java.util.List<Player> players = gameRoom.getPlayers();
                connectedPlayersListComponent.clearItems();
                for (Player player : players) {
                    connectedPlayersListComponent.getItems().add(player.getNickname());
                }

                playersCount = players.size();
                maxPlayers = gameRoom.getMaxPlayersCount();
                label.setText("Waiting for players: " + playersCount + "/" + maxPlayers);
            }
        });
    }

    @Override
    public void addButtons() {
    }
}
