package pl.vdudnyk.game.screen.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.List;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;

/**
 * Created by vadym on 23.10.17.
 */

public class Styles {
    public static final int DEFAULT_OFFSET = 10;

    private Skin skin;
    private TextButton.TextButtonStyle textButtonStyle;
    private Label.LabelStyle labelStyle;
    private List.ListStyle listStyle;

    public Styles() {
        this.skin = new Skin();
    }

    public void initStyles() {
        BitmapFont font = new BitmapFont();
        skin.add("default", font);

        skin.add("background_color", Color.WHITE, Color.class);

        Pixmap pixmap = new Pixmap(Gdx.graphics.getWidth() / 4, Gdx.graphics.getHeight() / 10, Pixmap.Format.RGB888);
        pixmap.setColor(Color.WHITE);
        pixmap.fill();
        skin.add("background", new Texture(pixmap));


        textButtonStyle = new TextButton.TextButtonStyle();
        textButtonStyle.up = skin.newDrawable("background", Color.GRAY);
        textButtonStyle.down = skin.newDrawable("background", Color.DARK_GRAY);
        textButtonStyle.over = skin.newDrawable("background", Color.LIGHT_GRAY);
        textButtonStyle.font = skin.getFont("default");
        skin.add("default", textButtonStyle);

        labelStyle = new Label.LabelStyle();
        labelStyle.background = skin.newDrawable("background", Color.GRAY);
        labelStyle.font = skin.getFont("default");
        skin.add("default", labelStyle);

        listStyle = new List.ListStyle();
        listStyle.background = skin.newDrawable("background", Color.BLACK);
        listStyle.font = skin.getFont("default");
        listStyle.selection = skin.newDrawable("background", Color.BLUE);
        listStyle.fontColorUnselected = Color.BLACK;
        skin.add("default", listStyle);
    }

    public Skin getSkin() {
        return skin;
    }

    public TextButton.TextButtonStyle getTextButtonStyle() {
        return textButtonStyle;
    }

    public Label.LabelStyle getLabelStyle() {
        return labelStyle;
    }

    public List.ListStyle getListStyle() {
        return listStyle;
    }
}
