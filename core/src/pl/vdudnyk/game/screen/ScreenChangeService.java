package pl.vdudnyk.game.screen;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vadym on 14.10.17.
 */
public class ScreenChangeService {
    private String currentScreenName;

    private List<ChangeScreenEventListener> subscriberList = new ArrayList<ChangeScreenEventListener>();

    public void setScreen(String screenName) {
        currentScreenName = screenName;
        for (ChangeScreenEventListener changeScreenEventListener : subscriberList) {
            changeScreenEventListener.screenChangeRequested(currentScreenName);
        }

    }

    public void subscribeOnScreenChangeEvent(ChangeScreenEventListener changeScreenEventListener) {
        subscriberList.add(changeScreenEventListener);
    }

    public interface ChangeScreenEventListener {
        void screenChangeRequested(String screenName);
    }
}
