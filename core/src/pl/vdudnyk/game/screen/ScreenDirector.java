package pl.vdudnyk.game.screen;

import com.badlogic.gdx.Game;
import pl.vdudnyk.game.screen.menu.BaseScreen;

import java.util.ArrayList;
import java.util.List;

public class ScreenDirector {

    private static ScreenDirector instance;

    private static List<BaseScreen> screenList = new ArrayList<BaseScreen>();
    private BaseScreen currentScreen;
    private Game game;

    private ScreenDirector() {

    }

    public static ScreenDirector getInstance() {
        if (instance == null) {
            instance = new ScreenDirector();
        }
        return instance;
    }

    public ScreenDirector initialize(Game game) {
        this.game = game;
        return this;
    }

    public <T extends BaseScreen> ScreenDirector add(T screen) {
        ScreenDirector.screenList.add(screen);
        return this;
    }

    public void setScreen(String screenName) {
        for (BaseScreen screen : screenList) {
            if (screen.screenName.equals(screenName)) {
                System.out.println("Setting screen: " + screen.screenName);
                this.currentScreen = screen;
                break;
            }
        }
    }

    public void switchScreenIfNeed() {
        if(!currentScreen.equals(game.getScreen())) {
            this.game.setScreen(currentScreen);
        }
    }
}
