package pl.vdudnyk.game.control.ui;

import com.badlogic.gdx.scenes.scene2d.Stage;
import pl.vdudnyk.game.actors.ActorFactory;
import pl.vdudnyk.game.actors.PlayerGroup;
import pl.vdudnyk.game.actors.ui.HealthBar;
import pl.vdudnyk.game.common.dto.gaming.Health;

public class UiController {
    private HealthBar healthBar;
    private Stage stage;

    public void initHealthBar(Stage stage) {
        this.stage = stage;
        healthBar = ActorFactory.createHealthBar();
        stage.addActor(healthBar);
    }


    public void updateUI(PlayerGroup userPlayerActorGroup) {
        updateHealthBar(userPlayerActorGroup);
    }

    private void updateHealthBar(PlayerGroup userPlayerActorGroup) {
        if(userPlayerActorGroup == null) {
            return;
        }
        Health health = userPlayerActorGroup.getPlayerActor().getPlayer().getPlayerState().getHealth();
        healthBar.setHealth(health);
        healthBar.setX(stage.getCamera().position.x + stage.getCamera().viewportWidth/2 - HealthBar.ACTOR_WIDTH - 10);
        healthBar.setY(stage.getCamera().position.y - stage.getCamera().viewportHeight/2 + 10);
    }
}
