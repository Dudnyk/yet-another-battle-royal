package pl.vdudnyk.game.control.camera;

import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.Stage;
import pl.vdudnyk.game.actors.PlayerGroup;

public class CameraController {
    public void updateCamera(float delta, Stage stage, PlayerGroup userPlayerActorGroup) {
        if (userPlayerActorGroup == null) {
            return;
        }

        int x = userPlayerActorGroup.getPlayerActor().getPlayer().getPlayerState().getPosition().getX();
        int y = userPlayerActorGroup.getPlayerActor().getPlayer().getPlayerState().getPosition().getY();
        float lerp = 1.5f;
        Vector3 position = stage.getCamera().position;
        position.x += (x - position.x) * lerp * delta;
        position.y += (y - position.y) * lerp * delta;
    }
}
