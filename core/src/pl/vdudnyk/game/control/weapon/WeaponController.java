package pl.vdudnyk.game.control.weapon;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Stage;
import pl.vdudnyk.game.actors.PlayerGroup;
import pl.vdudnyk.game.actors.weapon.WeaponActor;
import pl.vdudnyk.game.common.dto.Player;

import java.util.Map;

public class WeaponController {

    public void handle(Stage stage, Map<String, PlayerGroup> players) {
        //WEAPON

        for (PlayerGroup playerGroup : players.values()) {
            Player player = playerGroup.getPlayerActor().getPlayer();

            Vector2 userCentralPosition = new Vector2();
            playerGroup.getPlayerActor().getBounds().getCenter(userCentralPosition);

            Vector2 viewPortCentralPosition = new Vector2(stage.getViewport().getScreenWidth() / 2,
                    stage.getViewport().getScreenHeight() / 2);

            // Inverted mouse and player position as well as X and Y axis
            float angle = (float) Math.toDegrees(Math.atan2(player.getPlayerState().getMouseY() - (double) viewPortCentralPosition.y, player.getPlayerState().getMouseX() - (double) viewPortCentralPosition.x));
            WeaponActor weaponActor = playerGroup.getWeaponActor();
            weaponActor.setRotation(270 - angle);

            weaponActor.setX(userCentralPosition.x);
            weaponActor.setY(userCentralPosition.y);
        }

    }
}
