package pl.vdudnyk.game.control.scene;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.Scaling;
import pl.vdudnyk.game.configuration.MapConfiguration;

public class BackgroundController {

    private static final int BACKGROUND_EXPAND = 200;

    public void initBackground(Stage stage) {
        FileHandle backgroundImage = Gdx.files.internal("background/background.jpg");
        Texture texture = new Texture(backgroundImage);
        Image image = new Image(texture);
        image.setScaling(Scaling.fill);
        image.setHeight(MapConfiguration.mapHeigh + BACKGROUND_EXPAND * 2);
        image.setWidth(MapConfiguration.mapWidht + BACKGROUND_EXPAND);
        image.setX(-BACKGROUND_EXPAND);
        image.setY(-BACKGROUND_EXPAND);
        stage.addActor(image);
    }
}
