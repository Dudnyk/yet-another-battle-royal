package pl.vdudnyk.game.control.scene;


import com.badlogic.gdx.scenes.scene2d.Stage;
import pl.vdudnyk.game.configuration.MapConfiguration;
import pl.vdudnyk.game.actors.ActorFactory;
import pl.vdudnyk.game.actors.WallActor;

import java.util.ArrayList;
import java.util.List;

public class EnvironmentController {
    private Stage stage;
    private List<WallActor> wallActors = new ArrayList<WallActor>();

    public void initEnvironment(Stage stage) {
        this.stage = stage;
        createWalls();

    }

    private void createWalls() {
        //draw horizontal walls
        for (int i = 0; i <= MapConfiguration.mapWidht; i += WallActor.ACTOR_WIDTH) {
            WallActor wallActorBottom = ActorFactory.createWallActor(i, 0);
            WallActor wallActorTop = ActorFactory.createWallActor(i, MapConfiguration.mapHeigh);
            stage.addActor(wallActorBottom);
            stage.addActor(wallActorTop);
            wallActors.add(wallActorBottom);
            wallActors.add(wallActorTop);
        }

        //draw vertcal walls
        for (int i = 0; i <= MapConfiguration.mapHeigh; i += WallActor.ACTOR_HEIGHT) {
            WallActor wallActorLeft = ActorFactory.createWallActor(0, i);
            WallActor wallActorRight = ActorFactory.createWallActor(MapConfiguration.mapWidht, i);
            stage.addActor(wallActorLeft);
            stage.addActor(wallActorRight);
            wallActors.add(wallActorLeft);
            wallActors.add(wallActorRight);
        }

    }

    public List<WallActor> getWalls() {
        return wallActors;
    }
}
