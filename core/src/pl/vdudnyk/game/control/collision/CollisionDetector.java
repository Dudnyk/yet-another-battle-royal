package pl.vdudnyk.game.control.collision;

import com.badlogic.gdx.math.Rectangle;
import pl.vdudnyk.game.actors.BaseActor;

public class CollisionDetector {
    public static boolean isCollision(BaseActor firstActor, BaseActor secondActor) {
        return firstActor.getBounds().overlaps(secondActor.getBounds());
    }

    public static boolean isCollision(BaseActor firstActor, int x, int y, int wight, int height) {
        Rectangle firstActorBounds = firstActor.getBounds();
        Rectangle secondActorBounds = new Rectangle(x, y, wight, height);
        return firstActorBounds.overlaps(secondActorBounds);
    }
}
