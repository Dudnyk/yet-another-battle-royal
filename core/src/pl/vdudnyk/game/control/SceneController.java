package pl.vdudnyk.game.control;

import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.Stage;
import pl.vdudnyk.game.common.dto.GameWorld;
import pl.vdudnyk.game.common.dto.Player;
import pl.vdudnyk.game.control.camera.CameraController;
import pl.vdudnyk.game.control.player.PlayerController;
import pl.vdudnyk.game.control.scene.BackgroundController;
import pl.vdudnyk.game.control.scene.EnvironmentController;
import pl.vdudnyk.game.control.ui.UiController;
import pl.vdudnyk.game.control.weapon.WeaponController;
import pl.vdudnyk.game.network.gameroom.GameWorldRetrievedListener;
import pl.vdudnyk.game.network.gameroom.GameWorldService;
import pl.vdudnyk.game.network.gameroom.GameWorldUpdatedListener;
import pl.vdudnyk.game.network.gameroom.PlayerDataHolder;
import pl.vdudnyk.game.actors.ActorFactory;
import pl.vdudnyk.game.actors.PlayerGroup;
import pl.vdudnyk.game.screen.ui.Styles;

import java.util.HashMap;
import java.util.Map;

public class SceneController {

    private GameWorld gameWorld;
    private Stage stage;

    private PlayerController playerController;
    private GameWorldService gameWorldService;
    private BackgroundController backgroundController;
    private EnvironmentController environmentController;
    private UiController uiController;
    private CameraController cameraController;
    private WeaponController weaponController;
    private Styles styles;

    private boolean playersAdded = false;
    private HashMap<String, PlayerGroup> playerGroupHashMap = new HashMap<String, PlayerGroup>();
    private PlayerGroup userPlayerActorGroup;

    public SceneController(PlayerController playerController, GameWorldService gameWorldService, BackgroundController backgroundController, EnvironmentController environmentController, UiController uiController, CameraController cameraController, WeaponController weaponController, Styles styles) {
        this.playerController = playerController;
        this.gameWorldService = gameWorldService;
        this.backgroundController = backgroundController;
        this.environmentController = environmentController;
        this.uiController = uiController;
        this.cameraController = cameraController;
        this.weaponController = weaponController;
        this.styles = styles;
    }

    public void init(Stage stage) {
        this.stage = stage;
        backgroundController.initBackground(stage);
        environmentController.initEnvironment(stage);
        uiController.initHealthBar(stage);
        setUpListeners();
    }


    public void update(float delta) {
        handleGameWorldChange();
        playerController.handleControls(stage, userPlayerActorGroup, environmentController);
        gameWorldService.updateServerGameWorld(gameWorld, userPlayerActorGroup, gameWorldUpdatedListener);
        cameraController.updateCamera(delta, stage, userPlayerActorGroup);
        uiController.updateUI(userPlayerActorGroup);
        weaponController.handle(stage, playerGroupHashMap);
    }

    private void handleGameWorldChange() {
        if (gameWorld == null) {
            return;
        }
        if (!playersAdded) {
            addPlayers();
        }

        if (playersAdded) {
            updatePlayersState();
        }

    }

    private void addPlayers() {
        for (Player player : gameWorld.getPlayers().values()) {
            PlayerGroup playerGroup;
            if (player.getNickname().equals(PlayerDataHolder.getNickName())) {
                playerGroup = ActorFactory.createUserPlayerGroup(player, styles.getSkin());
                userPlayerActorGroup = playerGroup;

            } else {
                playerGroup = ActorFactory.createPlayerGroup(player, styles.getSkin());
            }

            stage.addActor(playerGroup);
            playerGroupHashMap.put(player.getNickname(), playerGroup);

        }
        playersAdded = true;
    }


    private void updatePlayersState() {
        Map<String, Player> players = gameWorld.getPlayers();
        for (String playerNickname : players.keySet()) {
            if (!playerNickname.equals(PlayerDataHolder.getNickName())) {
                playerGroupHashMap
                        .get(playerNickname)
                        .getPlayerActor()
                        .getPlayer()
                        .setPlayerState(players.get(playerNickname).getPlayerState());
            }
        }
    }


    private void setUpListeners() {
        gameWorldService.getGameWorld(new GameWorldRetrievedListener() {
            @Override
            public void gameWorldRetrieved(GameWorld gameWorld) {
                setGameWorld(gameWorld);
            }
        });
    }

    private GameWorldUpdatedListener gameWorldUpdatedListener = new GameWorldUpdatedListener() {
        @Override
        public void gameWorldUpdated(GameWorld gameWorld) {
            setGameWorld(gameWorld);
        }
    };

    private void setGameWorld(GameWorld gameWorld) {
        this.gameWorld = gameWorld;
    }

}
