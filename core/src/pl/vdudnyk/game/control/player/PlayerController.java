package pl.vdudnyk.game.control.player;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Stage;
import pl.vdudnyk.game.actors.weapon.WeaponActor;
import pl.vdudnyk.game.control.collision.CollisionDetector;
import pl.vdudnyk.game.control.scene.EnvironmentController;
import pl.vdudnyk.game.actors.PlayerGroup;
import pl.vdudnyk.game.actors.WallActor;

import java.awt.*;
import java.util.List;

import static pl.vdudnyk.game.actors.UserPlayerActor.MOVE_STEP;

public class PlayerController {

    public void handleControls(Stage stage, PlayerGroup playerGroup, EnvironmentController environmentController) {

        if (playerGroup == null) {
            return;
        }

        int x = playerGroup.getPlayerActor().getPlayer().getPlayerState().getPosition().getX();
        int y = playerGroup.getPlayerActor().getPlayer().getPlayerState().getPosition().getY();
        int width = (int) playerGroup.getPlayerActor().getWidth();
        int height = (int) playerGroup.getPlayerActor().getHeight();

        if (Gdx.input.isKeyPressed(Input.Keys.W)) {
            if (!isCollision(environmentController, x, y + MOVE_STEP, width, height)) {
                playerGroup.getPlayerActor().getPlayer().getPlayerState().getPosition().setXY(x, y + MOVE_STEP);
            }
        }
        if (Gdx.input.isKeyPressed(Input.Keys.S)) {
            if (!isCollision(environmentController, x, y - MOVE_STEP, width, height)) {
                playerGroup.getPlayerActor().getPlayer().getPlayerState().getPosition().setXY(x, y - MOVE_STEP);
            }
        }
        if (Gdx.input.isKeyPressed(Input.Keys.A)) {
            if (!isCollision(environmentController, x - MOVE_STEP, y, width, height)) {
                playerGroup.getPlayerActor().getPlayer().getPlayerState().getPosition().setXY(x - MOVE_STEP, y);
            }
        }
        if (Gdx.input.isKeyPressed(Input.Keys.D)) {
            if (!isCollision(environmentController, x + MOVE_STEP, y, width, height)) {
                playerGroup.getPlayerActor().getPlayer().getPlayerState().getPosition().setXY(x + MOVE_STEP, y);
            }
        }



    }

    private boolean isCollision(EnvironmentController environmentController, int x, int y, int wight, int height) {
        List<WallActor> walls = environmentController.getWalls();
        for (WallActor wall : walls) {
            if (CollisionDetector.isCollision(wall, x, y, wight, height)) {
                return true;
            }
        }
        return false;

    }


}
