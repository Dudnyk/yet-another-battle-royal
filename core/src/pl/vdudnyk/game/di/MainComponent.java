package pl.vdudnyk.game.di;

import dagger.Component;
import pl.vdudnyk.game.MainGame;

import javax.inject.Singleton;

/**
 * Created by vadym on 14.10.17.
 */
@Singleton
@Component(modules = MainModule.class)
public interface MainComponent {
    void inject(MainGame applicationListener);
}