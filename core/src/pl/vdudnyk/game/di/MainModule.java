package pl.vdudnyk.game.di;

import dagger.Module;
import dagger.Provides;
import pl.vdudnyk.game.MainGame;
import pl.vdudnyk.game.control.camera.CameraController;
import pl.vdudnyk.game.control.player.PlayerController;
import pl.vdudnyk.game.control.scene.BackgroundController;
import pl.vdudnyk.game.control.scene.EnvironmentController;
import pl.vdudnyk.game.control.SceneController;
import pl.vdudnyk.game.control.ui.UiController;
import pl.vdudnyk.game.control.weapon.WeaponController;
import pl.vdudnyk.game.network.NetworkClient;
import pl.vdudnyk.game.network.gameroom.GameRoomService;
import pl.vdudnyk.game.network.gameroom.GameWorldService;
import pl.vdudnyk.game.screen.ScreenChangeService;
import pl.vdudnyk.game.screen.menu.*;
import pl.vdudnyk.game.screen.ui.Styles;

import javax.inject.Singleton;

/**
 * Created by vadym on 14.10.17.
 */
@Module
public class MainModule {

    @Provides
    @Singleton
    MainGame provideGameEndpoint() {
        return new MainGame();
    }

    @Provides
    @Singleton
    MainMenu provideMainMenu(Styles styles) {
        return new MainMenu(styles);
    }

    @Provides
    @Singleton
    FindGameMenu provideFindGameMenu(NetworkClient networkClient, Styles styles) {
        return new FindGameMenu(networkClient, styles);
    }

    @Provides
    @Singleton
    GameRoomMenu provideGameRoomMenu(GameRoomService gameRoomService, Styles styles) {
        return new GameRoomMenu(styles, gameRoomService);
    }

    @Provides
    @Singleton
    GameScreen provideGameScreen(Styles styles, SceneController sceneController) {
        return new GameScreen(styles, sceneController);
    }

    @Provides
    @Singleton
    TestScreen provideTestScreen(NetworkClient networkClient, Styles styles) {
        return new TestScreen(networkClient, styles);
    }


    @Provides
    @Singleton
    ScreenChangeService provideScreenChangeService() {
        return new ScreenChangeService();
    }

    @Provides
    @Singleton
    NetworkClient provideNetworkClient() {
        return new NetworkClient();
    }

    @Provides
    @Singleton
    Styles provideStyles() {
        return new Styles();
    }

    @Provides
    @Singleton
    GameRoomService provideGameRoomService(NetworkClient networkClient) {
        return new GameRoomService(networkClient);
    }

    @Provides
    @Singleton
    GameWorldService provideGameWorldService(NetworkClient networkClient) {
        return new GameWorldService(networkClient);
    }

    @Provides
    @Singleton
    PlayerController providePlayerController() {
        return new PlayerController();
    }

    @Provides
    @Singleton
    SceneController provideSceneController(PlayerController playerController,
                                           GameWorldService gameWorldService,
                                           BackgroundController backgroundController,
                                           EnvironmentController environmentController,
                                           UiController uiController,
                                           CameraController cameraController,
                                           WeaponController weaponController,
                                           Styles styles) {
        return new SceneController(
                playerController,
                gameWorldService,
                backgroundController,
                environmentController,
                uiController,
                cameraController,
                weaponController,
                styles);
    }

    @Provides
    @Singleton
    BackgroundController provideBackgroundController() {
        return new BackgroundController();
    }

    @Provides
    @Singleton
    EnvironmentController provideEnvironmentController() {
        return new EnvironmentController();
    }

    @Provides
    @Singleton
    UiController provideUiController() {
        return new UiController();
    }

    @Provides
    @Singleton
    CameraController provideCameraController() {
        return new CameraController();
    }

    @Provides
    @Singleton
    WeaponController provideWeaponController() {
        return new WeaponController();
    }
}