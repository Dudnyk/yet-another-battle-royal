package pl.vdudnyk.game;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import pl.vdudnyk.game.di.DaggerMainComponent;
import pl.vdudnyk.game.di.MainModule;
import pl.vdudnyk.game.screen.ScreenChangeService;
import pl.vdudnyk.game.screen.ScreenDirector;
import pl.vdudnyk.game.screen.menu.*;
import pl.vdudnyk.game.screen.ui.Styles;

import javax.inject.Inject;

/**
 * Created by vadym on 14.10.17.
 */
public class MainGame extends Game {
    private final pl.vdudnyk.game.di.MainComponent component;

    @Inject
    ScreenChangeService screenChangeService;

    @Inject
    MainMenu mainMenu;

    @Inject
    FindGameMenu findGameMenu;

    @Inject
    GameRoomMenu gameRoomMenu;

    @Inject
    GameScreen gameScreen;

    @Inject
    TestScreen testScreen;

    @Inject
    Styles styles;

    public MainGame() {
        component = DaggerMainComponent.builder().mainModule(new MainModule()).build();
        component.inject(this);

        ScreenDirector.getInstance()
                .initialize(this)
        .add(mainMenu)
        .add(findGameMenu)
        .add(gameRoomMenu)
        .add(gameScreen)
        .add(testScreen);

    }

    @Override
    public void create() {
        styles.initStyles();
        ScreenDirector.getInstance().setScreen(MainMenu.SCREEN_NAME);
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void render() {
        ScreenDirector.getInstance().switchScreenIfNeed();
        getScreen().render(Gdx.graphics.getDeltaTime());

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void dispose() {

    }
}
