package pl.vdudnyk.game.network;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryonet.Client;
import pl.vdudnyk.game.common.ObjectRegistry;
import pl.vdudnyk.game.common.request.BaseRequest;

import java.io.IOException;

/**
 * Created by vadym on 12.10.17.
 */
public class NetworkClient {
    private Client client;

    public NetworkClient() {
        client = setupClient();
    }

    private Client setupClient() {
        client = new Client();
        Kryo kryo = client.getKryo();
        new ObjectRegistry().register(kryo);
        client.start();
        try {
            client.connect(300, "127.0.0.1", 54555, 54777);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return client;
    }

    public void sendRequest(BaseRequest request) {
        client.sendTCP(request);
    }

    public Client getClient() {
        return client;
    }

    public void dispose() {
        client.close();
    }
}
