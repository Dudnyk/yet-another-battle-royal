package pl.vdudnyk.game.network.gameroom;

import com.badlogic.gdx.Gdx;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import pl.vdudnyk.game.common.dto.GameWorld;
import pl.vdudnyk.game.common.dto.Player;
import pl.vdudnyk.game.common.request.GetGameWorldRequest;
import pl.vdudnyk.game.common.request.UpdateGameWorldRequest;
import pl.vdudnyk.game.common.response.GetGameWorldResponse;
import pl.vdudnyk.game.common.response.UpdateGameWorldResponse;
import pl.vdudnyk.game.network.NetworkClient;
import pl.vdudnyk.game.actors.PlayerGroup;

/**
 * Created by vadym on 28.11.17.
 */
public class GameWorldService {
    private NetworkClient networkClient;

    public GameWorldService(NetworkClient networkClient) {
        this.networkClient = networkClient;
    }

    public void getGameWorld(final GameWorldRetrievedListener gameWorldRetrievedListener) {
        GetGameWorldRequest request = new GetGameWorldRequest();
        request.setGameRoom(PlayerDataHolder.getGameRoom());

        networkClient.getClient().addListener(new Listener() {
            @Override
            public void received(Connection connection, Object object) {
                super.received(connection, object);
                if (object instanceof GetGameWorldResponse) {
                    GetGameWorldResponse response = (GetGameWorldResponse) object;
                    System.out.println("Found Game World, id: " + response.getGameWorld().getGameWorldId());
                    gameWorldRetrievedListener.gameWorldRetrieved(response.getGameWorld());
                }
            }

        });
        networkClient.getClient().sendTCP(request);
    }

    public void updateServerGameWorld(GameWorld gameWorld, PlayerGroup userPlayerGroup, final GameWorldUpdatedListener gameWorldUpdatedListener) {
        if(gameWorld == null) {
            return;
        }

        if(userPlayerGroup == null) {
            return;
        }

        Player player = userPlayerGroup.getPlayerActor().getPlayer();

        //position change logic
        gameWorld.getPlayers().get(player.getNickname()).setPlayerState(player.getPlayerState());
        gameWorld.getPlayers().get(player.getNickname()).getPlayerState().setMouseX(Gdx.input.getX());
        gameWorld.getPlayers().get(player.getNickname()).getPlayerState().setMouseY(Gdx.input.getY());

        UpdateGameWorldRequest request = new UpdateGameWorldRequest();
        request.setGameWorld(gameWorld);

        networkClient.getClient().addListener(new Listener() {
            @Override
            public void received(Connection connection, Object object) {
                super.received(connection, object);
                if (object instanceof UpdateGameWorldResponse) {
                    UpdateGameWorldResponse response = (UpdateGameWorldResponse) object;
                    gameWorldUpdatedListener.gameWorldUpdated(response.getGameWorld());
                }
            }

        });

        networkClient.getClient().sendTCP(request);
    }
}