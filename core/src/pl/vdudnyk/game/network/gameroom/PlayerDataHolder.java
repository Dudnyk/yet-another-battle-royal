package pl.vdudnyk.game.network.gameroom;

import pl.vdudnyk.game.common.dto.GameRoom;
import pl.vdudnyk.game.common.dto.GameWorld;

/**
 * Created by vadym on 28.11.17.
 */
public class PlayerDataHolder {
    private PlayerDataHolder() {

    }

    private static GameRoom gameRoom;
    private static GameWorld gameWorld;
    private static String nickName;

    public static GameRoom getGameRoom() {
        return gameRoom;
    }

    public static void setGameRoom(GameRoom gameRoom) {
        PlayerDataHolder.gameRoom = gameRoom;
    }

    public static GameWorld getGameWorld() {
        return gameWorld;
    }

    public static void setGameWorld(GameWorld gameWorld) {
        PlayerDataHolder.gameWorld = gameWorld;
    }

    public static String getNickName() {
        return nickName;
    }

    public static void setNickName(String nickName) {
        PlayerDataHolder.nickName = nickName;
    }
}
