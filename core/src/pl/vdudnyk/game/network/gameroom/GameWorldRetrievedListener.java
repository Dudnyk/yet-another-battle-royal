package pl.vdudnyk.game.network.gameroom;

import pl.vdudnyk.game.common.dto.GameWorld;

/**
 * Created by vadym on 28.11.17.
 */
public interface GameWorldRetrievedListener {
    void gameWorldRetrieved(GameWorld gameWorld);
}
