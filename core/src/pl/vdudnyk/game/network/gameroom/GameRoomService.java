package pl.vdudnyk.game.network.gameroom;

import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import pl.vdudnyk.game.common.request.ConnectToGameRoomRequest;
import pl.vdudnyk.game.common.response.BaseResponse;
import pl.vdudnyk.game.common.response.ConnectToGameRoomResponse;
import pl.vdudnyk.game.common.response.StartGameResponse;
import pl.vdudnyk.game.network.NetworkClient;

/**
 * Created by vadym on 23.10.17.
 */
public class GameRoomService {

    private NetworkClient networkClient;

    public GameRoomService(NetworkClient networkClient) {
        this.networkClient = networkClient;
    }

    public void findGameRoom(String nickname, final GameRoomFoundListener gameRoomFoundListener) {
        ConnectToGameRoomRequest request = new ConnectToGameRoomRequest();
        request.setNickname(nickname);
        networkClient.sendRequest(request);
        networkClient.getClient().addListener(new Listener() {
            @Override
            public void received(Connection connection, Object object) {
                super.received(connection, object);
                if (object instanceof ConnectToGameRoomResponse) {
                    ConnectToGameRoomResponse response = (ConnectToGameRoomResponse) object;
                    System.out.println("Found game room, id: " + response.getGameRoom().getGameRoomId());
                    gameRoomFoundListener.gameRoomFound(response.getGameRoom());
                }
            }
        });
    }

    public void subscribeForGameStart(final StartGameListener startGameListener) {
        networkClient.getClient().addListener(new Listener() {
            @Override
            public void received(Connection connection, Object object) {
                if (object instanceof StartGameResponse) {
                    StartGameResponse response = (StartGameResponse) object;
                    System.out.println("Found game room, id: " + response.getGameRoom().getGameRoomId());
                    startGameListener.gameStarted(response.getGameRoom());
                }
            }
        });
    }
}
