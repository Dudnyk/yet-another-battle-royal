package pl.vdudnyk.game.network.gameroom;

import pl.vdudnyk.game.common.dto.GameRoom;

/**
 * Created by vadym on 28.11.17.
 */
public interface StartGameListener {
    void gameStarted(GameRoom gameRoom);
}
