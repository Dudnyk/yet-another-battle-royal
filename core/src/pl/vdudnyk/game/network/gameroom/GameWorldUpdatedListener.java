package pl.vdudnyk.game.network.gameroom;

import pl.vdudnyk.game.common.dto.GameWorld;

public interface GameWorldUpdatedListener {
    void gameWorldUpdated(GameWorld gameWorld);
}
