package pl.vdudnyk.game.network.gameroom;

import pl.vdudnyk.game.common.dto.GameRoom;

/**
 * Created by vadym on 23.10.17.
 */
public interface GameRoomFoundListener {
    void gameRoomFound(GameRoom gameRoom);
}
