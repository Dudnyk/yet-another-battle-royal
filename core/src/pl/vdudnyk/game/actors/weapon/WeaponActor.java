package pl.vdudnyk.game.actors.weapon;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import pl.vdudnyk.game.actors.MovableActor;

public class WeaponActor extends MovableActor {
    protected TextureRegion textureRegion;

    public static final int ACTOR_WIDTH = 16;
    public static final int ACTOR_HEIGHT = 64;

    public WeaponActor() {
        textureRegion = new TextureRegion(new Texture(Gdx.files.internal("weapons/weapon1.png")),0 , 0, ACTOR_WIDTH, ACTOR_HEIGHT);
        setWidth(ACTOR_WIDTH);
        setHeight(ACTOR_HEIGHT);
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        batch.draw(textureRegion, getX(), getY(), getOriginX(), getOriginY(), getWidth(), getHeight(), getScaleX(), getScaleY(), getRotation());
        super.draw(batch, parentAlpha);
    }
}
