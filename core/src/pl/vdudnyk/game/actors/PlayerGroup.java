package pl.vdudnyk.game.actors;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Group;
import pl.vdudnyk.game.actors.weapon.WeaponActor;

public class PlayerGroup extends Group {
    private PlayerActor playerActor;
    private PlayerNicknameActor playerNicknameActor;
    private WeaponActor weaponActor;

    public PlayerGroup(PlayerActor playerActor, PlayerNicknameActor playerNicknameActor, WeaponActor weaponActor) {
        this.playerActor = playerActor;
        this.playerNicknameActor = playerNicknameActor;
        this.weaponActor = weaponActor;
        this.addActor(playerActor);
        this.addActor(playerNicknameActor);
        this.addActor(weaponActor);
    }

    public PlayerActor getPlayerActor() {
        return playerActor;
    }

    public PlayerNicknameActor getPlayerNicknameActor() {
        return playerNicknameActor;
    }

    public WeaponActor getWeaponActor() {
        return weaponActor;
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        getPlayerNicknameActor()
                .setPosition(playerActor.player.getPlayerState().getPosition().getX(),
                        playerActor.player.getPlayerState().getPosition().getY());
        super.draw(batch, parentAlpha);
    }


}
