package pl.vdudnyk.game.actors;

import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Actor;

public abstract class BaseActor extends Actor {

    protected Rectangle bounds;

    public abstract Rectangle getBounds ();


}
