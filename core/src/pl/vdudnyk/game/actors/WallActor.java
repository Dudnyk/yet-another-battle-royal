package pl.vdudnyk.game.actors;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;

public class WallActor extends pl.vdudnyk.game.actors.StaticActor {
    protected Texture texture;
    public static final int ACTOR_WIDTH = 32;
    public static final int ACTOR_HEIGHT = 32;

    public WallActor(int x, int y) {
        super(x,y,ACTOR_WIDTH, ACTOR_HEIGHT);
        texture = new Texture(Gdx.files.internal("environment/wall.png"));
        this.setWidth(ACTOR_WIDTH);
        this.setHeight(ACTOR_HEIGHT);
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        batch.draw(texture, getX(), getY(), getWidth(), getHeight());
        super.draw(batch, parentAlpha);
    }
}
