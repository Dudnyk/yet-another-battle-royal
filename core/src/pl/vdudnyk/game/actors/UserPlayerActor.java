package pl.vdudnyk.game.actors;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import pl.vdudnyk.game.common.dto.Player;

public class UserPlayerActor extends pl.vdudnyk.game.actors.PlayerActor {
    public static final int MOVE_STEP = 5;

    public UserPlayerActor(Player player) {
        super(player);
        texture = new Texture(Gdx.files.internal("user_player1.png"));
    }


}
