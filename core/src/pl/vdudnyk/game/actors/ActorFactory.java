package pl.vdudnyk.game.actors;

import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import pl.vdudnyk.game.actors.ui.HealthBar;
import pl.vdudnyk.game.actors.weapon.WeaponActor;
import pl.vdudnyk.game.common.dto.Player;

public class ActorFactory {

    private ActorFactory() {

    }

    public static pl.vdudnyk.game.actors.PlayerActor createPlayerActor(Player player) {
        return new pl.vdudnyk.game.actors.PlayerActor(player);
    }

    public static UserPlayerActor createUserPlayerActor(Player player) {
        return new UserPlayerActor(player);
    }

    public static PlayerNicknameActor playerNicknameActor(Player player, Skin skin) {
        return new PlayerNicknameActor(player.getNickname(), skin);
    }

    public static PlayerGroup createPlayerGroup(Player player, Skin skin) {
        return new PlayerGroup(createPlayerActor(player), playerNicknameActor(player, skin), createWeaponActor());
    }

    public static PlayerGroup createUserPlayerGroup(Player player, Skin skin) {
        return new PlayerGroup(createUserPlayerActor(player), playerNicknameActor(player, skin), createWeaponActor());
    }

    public static WeaponActor createWeaponActor() {
        return new WeaponActor();
    }

    public static WallActor createWallActor(int x, int y) {
        return new WallActor(x, y);
    }

    public static HealthBar createHealthBar() {
        return new HealthBar();
    }
}
