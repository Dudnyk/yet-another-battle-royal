package pl.vdudnyk.game.actors.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import pl.vdudnyk.game.common.dto.gaming.Health;

public class HealthBar extends Actor {
    private Health health;
    private Texture emptyHealthTexture;
    private Texture currentHealthTexture;

    public static final int ACTOR_WIDTH = 32;
    public static final int ACTOR_HEIGHT = 256;

    public HealthBar() {
        emptyHealthTexture = new Texture(Gdx.files.internal("ui/pink.png"));
        currentHealthTexture = new Texture(Gdx.files.internal("ui/red.png"));
        setWidth(ACTOR_WIDTH);
        setHeight(ACTOR_HEIGHT);
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        drawHealth(batch, parentAlpha);
        super.draw(batch, parentAlpha);
    }

    private void drawHealth(Batch batch, float parentAlpha) {
        if (health == null) {
            return;
        }
        int max = health.getMax();
        int current = health.getCurrent();

        batch.draw(emptyHealthTexture, getX(), getY(), getWidth(), getHeight());
        batch.draw(currentHealthTexture, getX(), getY(), getWidth(), (getHeight() * current / 100));


    }

    public void setHealth(Health health) {
        this.health = health;
    }
}
