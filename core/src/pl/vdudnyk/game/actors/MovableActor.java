package pl.vdudnyk.game.actors;

import com.badlogic.gdx.math.Rectangle;

public class MovableActor extends pl.vdudnyk.game.actors.BaseActor {

    public Rectangle getBounds() {
        return new Rectangle(getX(), getY(), getWidth(), getHeight());
    }
}
