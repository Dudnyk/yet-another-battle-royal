package pl.vdudnyk.game.actors;

import com.badlogic.gdx.math.Rectangle;

public class StaticActor extends pl.vdudnyk.game.actors.BaseActor {

    public StaticActor(int x, int y, int width, int height) {
        this.bounds = new Rectangle(x, y, width, height);
        setPosition(x, y);
    }


    @Override
    public Rectangle getBounds() {
        return bounds;
    }
}
