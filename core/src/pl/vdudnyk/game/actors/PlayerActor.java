package pl.vdudnyk.game.actors;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import pl.vdudnyk.game.common.dto.Player;

/**
 * Created by vadym on 28.11.17.
 */
public class PlayerActor extends MovableActor {
    protected Texture texture;

    protected Player player;

    public PlayerActor(Player player) {
        this.player = player;
        texture = new Texture(Gdx.files.internal("player1.png"));
        setWidth(64);
        setHeight(64);
    }

    @Override
    public void draw(Batch batch, float alpha) {
        setX(player.getPlayerState().getPosition().getX());
        setY(player.getPlayerState().getPosition().getY());
        batch.draw(texture, getX(), getY());
    }


    public Player getPlayer() {
        return player;
    }

}
