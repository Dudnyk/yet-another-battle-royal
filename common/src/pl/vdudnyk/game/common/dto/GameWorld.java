package pl.vdudnyk.game.common.dto;

import java.util.Map;

/**
 * Created by vadym on 28.11.17.
 */
public class GameWorld {
    private Map<String, Player> players;
    private long gameWorldId;

    public Map<String, Player> getPlayers() {
        return players;
    }

    public void setPlayers(Map<String, Player> players) {
        this.players = players;
    }

    public long getGameWorldId() {
        return gameWorldId;
    }

    public void setGameWorldId(long gameWorldId) {
        this.gameWorldId = gameWorldId;
    }
}
