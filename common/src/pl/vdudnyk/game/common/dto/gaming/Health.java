package pl.vdudnyk.game.common.dto.gaming;

/**
 * Created by vadym on 28.11.17.
 */
public class Health {
    private int max;
    private int current;

    public int getMax() {
        return max;
    }

    public void setMax(int max) {
        this.max = max;
    }

    public int getCurrent() {
        return current;
    }

    public void setCurrent(int current) {
        this.current = current;
    }
}
