package pl.vdudnyk.game.common.dto;

import pl.vdudnyk.game.common.dto.gaming.PlayerState;

/**
 * Created by vadym on 23.10.17.
 */
public class Player {
    private String playerID;
    private int connectionID;
    private String nickname;
    private PlayerState playerState;

    public String getPlayerID() {
        return playerID;
    }

    public void setPlayerID(String playerID) {
        this.playerID = playerID;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public int getConnectionID() {
        return connectionID;
    }

    public void setConnectionID(int connectionID) {
        this.connectionID = connectionID;
    }

    public PlayerState getPlayerState() {
        return playerState;
    }

    public void setPlayerState(PlayerState playerState) {
        this.playerState = playerState;
    }
}
