package pl.vdudnyk.game.common.dto;

import java.util.List;

/**
 * Created by vadym on 23.10.17.
 */
public class GameRoom {
    private long gameRoomId;
    private int maxPlayersCount;
    private List<Player> players;

    public int getMaxPlayersCount() {
        return maxPlayersCount;
    }

    public void setMaxPlayersCount(int maxPlayersCount) {
        this.maxPlayersCount = maxPlayersCount;
    }

    public long getGameRoomId() {
        return gameRoomId;
    }

    public void setGameRoomId(long gameRoomId) {
        this.gameRoomId = gameRoomId;
    }

    public List<Player> getPlayers() {
        return players;
    }

    public void setPlayers(List<Player> players) {
        this.players = players;
    }
}
