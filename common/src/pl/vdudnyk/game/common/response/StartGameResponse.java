package pl.vdudnyk.game.common.response;

import pl.vdudnyk.game.common.dto.GameRoom;

/**
 * Created by vadym on 28.11.17.
 */
public class StartGameResponse extends BaseResponse {
    private GameRoom gameRoom;

    public GameRoom getGameRoom() {
        return gameRoom;
    }

    public void setGameRoom(GameRoom gameRoom) {
        this.gameRoom = gameRoom;
    }
}
