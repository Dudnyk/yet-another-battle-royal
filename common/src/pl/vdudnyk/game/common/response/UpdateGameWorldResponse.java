package pl.vdudnyk.game.common.response;

import pl.vdudnyk.game.common.dto.GameWorld;

public class UpdateGameWorldResponse {
    private GameWorld gameWorld;

    public GameWorld getGameWorld() {
        return gameWorld;
    }

    public void setGameWorld(GameWorld gameWorld) {
        this.gameWorld = gameWorld;
    }
}
