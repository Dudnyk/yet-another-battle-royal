package pl.vdudnyk.game.common.response;

import pl.vdudnyk.game.common.Status;

import java.util.Date;

/**
 * Created by vadym on 12.10.17.
 */
public class BaseResponse {
    Date date;
    Status status;

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }
}
