package pl.vdudnyk.game.common.response;

import pl.vdudnyk.game.common.dto.GameWorld;

/**
 * Created by vadym on 28.11.17.
 */
public class GetGameWorldResponse extends BaseResponse {
    private GameWorld gameWorld;

    public GameWorld getGameWorld() {
        return gameWorld;
    }

    public void setGameWorld(GameWorld gameWorld) {
        this.gameWorld = gameWorld;
    }
}
