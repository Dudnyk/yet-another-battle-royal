package pl.vdudnyk.game.common.request;

/**
 * Created by vadym on 23.10.17.
 */
public class ConnectToGameRoomRequest extends BaseRequest {
    private String nickname;

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }
}
