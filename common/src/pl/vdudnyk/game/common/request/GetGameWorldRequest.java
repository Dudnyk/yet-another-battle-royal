package pl.vdudnyk.game.common.request;

import pl.vdudnyk.game.common.dto.GameRoom;

/**
 * Created by vadym on 28.11.17.
 */
public class GetGameWorldRequest extends BaseRequest {
    private GameRoom gameRoom;

    public GameRoom getGameRoom() {
        return gameRoom;
    }

    public void setGameRoom(GameRoom gameRoom) {
        this.gameRoom = gameRoom;
    }
}
