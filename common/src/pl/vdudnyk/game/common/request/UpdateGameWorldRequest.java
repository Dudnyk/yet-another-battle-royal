package pl.vdudnyk.game.common.request;

import pl.vdudnyk.game.common.dto.GameWorld;

public class UpdateGameWorldRequest {
    private GameWorld gameWorld;

    public GameWorld getGameWorld() {
        return gameWorld;
    }

    public void setGameWorld(GameWorld gameWorld) {
        this.gameWorld = gameWorld;
    }
}
