package pl.vdudnyk.game.common.request;

import java.util.Date;

/**
 * Created by vadym on 12.10.17.
 */
public class BaseRequest {
    private Date date;

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

}
