package pl.vdudnyk.game.common;

import com.esotericsoftware.kryo.Kryo;
import pl.vdudnyk.game.common.dto.GameRoom;
import pl.vdudnyk.game.common.dto.GameWorld;
import pl.vdudnyk.game.common.dto.Player;
import pl.vdudnyk.game.common.dto.gaming.Health;
import pl.vdudnyk.game.common.dto.gaming.Inventory;
import pl.vdudnyk.game.common.dto.gaming.PlayerState;
import pl.vdudnyk.game.common.dto.gaming.Position;
import pl.vdudnyk.game.common.request.*;
import pl.vdudnyk.game.common.response.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

/**
 * Created by vadym on 12.10.17.
 */
public class ObjectRegistry {
    public void register(Kryo kryo) {
        kryo.register(BaseRequest.class);
        kryo.register(BaseResponse.class);
        kryo.register(Status.class);
        kryo.register(GetServerStatusRequest.class);
        kryo.register(GetServerStatusResponse.class);
        kryo.register(Date.class);
        kryo.register(GameRoom.class);
        kryo.register(Player.class);
        kryo.register(ConnectToGameRoomRequest.class);
        kryo.register(ConnectToGameRoomResponse.class);
        kryo.register(ArrayList.class);
        kryo.register(HashMap.class);
        kryo.register(StartGameResponse.class);
        kryo.register(GetGameWorldRequest.class);
        kryo.register(GetGameWorldResponse.class);
        kryo.register(GameWorld.class);
        kryo.register(UpdateGameWorldRequest.class);
        kryo.register(UpdateGameWorldResponse.class);

        kryo.register(PlayerState.class);
        kryo.register(Inventory.class);
        kryo.register(Position.class);
        kryo.register(Health.class);
    }
}
